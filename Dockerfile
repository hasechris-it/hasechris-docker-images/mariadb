FROM mariadb:latest
MAINTAINER hasechris <hase.christian92@gmail.com>

# remove skip-name-resolution from mariadb config
RUN sed -i /skip-name-resolve/d /etc/mysql/conf.d/docker.cnf
